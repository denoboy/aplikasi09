package id.ac.prasetyo.dendy.appx9

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    lateinit var db : SQLiteDatabase
    lateinit var lsAdapter : ListAdapter
    lateinit var mediaController : MediaController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DBOpenHelper(this).writableDatabase

        mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)

        lv.setOnItemClickListener(itemClick)

    }

    fun showDataVideo(){
        val cursor : Cursor = db.query("video", arrayOf("id_video as _id","id_cover","title"),
            null,null,null,null,"title asc")
        lsAdapter = SimpleCursorAdapter(this,R.layout.item_data_video,cursor,
            arrayOf("_id","id_cover","title"), intArrayOf(R.id.txIdVideo, R.id.txIdCover, R.id.txVideoTitle),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lv.adapter = lsAdapter
    }

    override fun onStart() {
        super.onStart()
        showDataVideo()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        txJudulFilm.setText("Now Playing : "+c.getString(c.getColumnIndex("title")))
        imV.setImageResource(c.getInt(c.getColumnIndex("id_cover")))
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }

}
