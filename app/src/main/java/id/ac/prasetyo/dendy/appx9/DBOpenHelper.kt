package id.ac.prasetyo.dendy.appx9

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (contet : Context) : SQLiteOpenHelper(contet, DB_NAME, null, DB_VER) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tVideo = "create table video(id_video text not null, id_cover text not null, title text not null)"
        val insert = "insert into video values('0x7f0b0000','0x7f060055','Dilan 1990')," +
                "('0x7f0b0002','0x7f060067','Yowis Ben - Gaiso Turu')," +
                "('0x7f0b0001','0x7f060064','Puisi Patrik')"


        db?.execSQL(tVideo)
        db?.execSQL(insert)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_NAME = "video"
        val DB_VER = 1
    }
}